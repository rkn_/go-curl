package main

import (
	"flag"
	"go-curl/gitutils"
)

func main() {
	flag.Parse()
	cmd := flag.Args()[0]
	switch cmd {
	case "pull":
		user := flag.Args()[1]
		gitutils.Pull(user)
	case "setup":
		user := flag.Args()[1]
		gitutils.Clone(user)
	}
}
