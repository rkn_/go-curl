package gitutils

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
)

// Clone call git clone command
func Clone(user string) {
	base := "https://eagle4.fu.is.saga-u.ac.jp/test/self_learning.git"
	repo := "https://eagle4.fu.is.saga-u.ac.jp/" + user + "/self_learning.git"
	prev, _ := filepath.Abs(".")
	dir := prev + "/self_learning/"
	cmd := exec.Command("git", "clone", repo)
	cmd.Run()
	os.Chdir(dir)
	cmd = exec.Command("git", "remote", "add", "upstream", base)
	cmd.Run()
	os.Chdir(prev)
	fmt.Println(prev)
	copy(prev+"/git-extends", dir+"git-extends")
	del(prev + "/git-extends")
	os.Chmod(dir+"git-extends", 755)
}

func copy(from, to string) {
	src, err := os.Open(from)
	must(err)
	defer src.Close()

	dist, _ := os.Create(to)
	must(err)
	defer dist.Close()

	_, err = io.Copy(dist, src)
}

func del(target string) {
	os.Remove(target)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
