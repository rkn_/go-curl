package gitutils

import (
	"bytes"
	"net/http"
	"os/exec"
)

// Pull is one of git-extends function
func Pull(user string) {
	cmd := exec.Command("git", "fetch", "upstream")
	cmd.Run()
	cmd = exec.Command("git", "merge", "upstream/master")
	cmd.Run()
	link := "https://eagle4.fu.is.saga-u.ac.jp/gogs-ether/api/addPull"
	param := "user=" + user
	req, _ := http.NewRequest(http.MethodPost, link, bytes.NewBufferString(param))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := &http.Client{}
	client.Do(req)
}
