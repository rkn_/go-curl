package gitutils

import (
	"os/exec"
)

// Push is one of git-extends function
func Push() {
	cmd := exec.Command("git", "push", "origin", "master")
	cmd.Run()
}
